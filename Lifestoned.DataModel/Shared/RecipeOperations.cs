﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lifestoned.DataModel.Shared
{
    public enum RecipeSkillCheck
    {
        [Display(Name = "0 - Default")]
        Default = 0,

        [Display(Name = "1 - Tinker")]
        Tinker = 1,

        [Display(Name = "2 - Imbue")]
        Imbue = 2
    }

    public enum StatModOperation
    {
        [Display(Name = "0 - Invalid")]
        Invalid = 0,

        [Display(Name = "1 - Assign")]
        Assign = 1,

        [Display(Name = "2 - Add")]
        Add = 2,

        [Display(Name = "3 - Copy to Target")]
        CopyTarget = 3,

        [Display(Name = "4 - Copy to Created")]
        CopyCreated = 4,
    }

    public enum StatReqOperation
    {
        [Display(Name = "0 - Invalid")]
        Invalid = 0,

        [Display(Name = "1 - Greater")]
        Greater = 1,

        [Display(Name = "2 - Greater or Equal")]
        GreaterEqual = 2,

        [Display(Name = "3 - Less")]
        Less = 3,

        [Display(Name = "4 - Less or Equal")]
        LessEqual = 4,

        [Display(Name = "5 - Equal")]
        Equal = 5,

        [Display(Name = "6 - Not Equal")]
        NotEqual = 6,

        [Display(Name = "7 - Exists")]
        Exists = 7,

        [Display(Name = "8 - Not Exists")]
        NotExists = 8,

    }
}
