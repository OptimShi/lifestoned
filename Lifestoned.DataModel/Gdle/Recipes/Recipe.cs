﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Lifestoned.DataModel.Shared;

namespace Lifestoned.DataModel.Gdle.Recipes
{
    public class Recipe : IMetadata
    {
        [JsonProperty("key")]
        public uint Key { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("recipe")]
        public RecipeData RecipeData { get; set; } = new RecipeData();

        [JsonProperty("precursors")]
        public List<RecipePrecursor> Precursors { get; set; } = new List<RecipePrecursor>();

        #region Metadata

        [JsonProperty("lastModified")]
        [Display(Name = "Last Modified Date")]
        public DateTime? LastModified { get; set; }

        [JsonProperty("modifiedBy")]
        [Display(Name = "Last Modified By")]
        public string ModifiedBy { get; set; }

        [JsonProperty("changelog")]
        public List<ChangelogEntry> Changelog { get; set; } = new List<ChangelogEntry>();

        [JsonProperty("userChangeSummary")]
        public string UserChangeSummary { get; set; }

        [JsonProperty("isDone")]
        [Display(Name = "Is Done")]
        public bool IsDone { get; set; }

        #endregion
    }

    public class RecipePrecursor
    {
        [JsonProperty("tool")]
        public uint Tool { get; set; }

        [JsonProperty("target")]
        public uint Target { get; set; }
    }

    public class RecipeData
    {
        public uint RecipeID { get; set; }

        // make enum?
        public int Skill { get; set; }
        public int SkillCheckFormulaType { get; set; }

        public int DataID { get; set; }
        public int Difficulty { get; set; }

        public uint SuccessWcid { get; set; }
        public int SuccessAmount { get; set; }
        public string SuccessMessage { get; set; }
        public int SuccessConsumeTargetAmount { get; set; }
        public float SuccessConsumeTargetChance { get; set; }
        public string SuccessConsumeTargetMessage { get; set; }
        public int SuccessConsumeToolAmount { get; set; }
        public float SuccessConsumeToolChance { get; set; }
        public string SuccessConsumeToolMessage { get; set; }

        public uint FailWcid { get; set; }
        public int FailAmount { get; set; }
        public string FailMessage { get; set; }
        public int FailureConsumeTargetAmount { get; set; }
        public float FailureConsumeTargetChance { get; set; }
        public string FailureConsumeTargetMessage { get; set; }
        public int FailureConsumeToolAmount { get; set; }
        public float FailureConsumeToolChance { get; set; }
        public string FailureConsumeToolMessage { get; set; }

        public int Unknown { get; set; }

        public RecipeMod[] Mods { get; set; } = new RecipeMod[8];
        public RecipeReq[] Requirements { get; set; } = new RecipeReq[3];
    }

    public class StatMod<TStat, TValue>
    {
        public int Unknown { get; set; }
        public int OperationType { get; set; }
        public TStat Stat { get; set; }
        public TValue Value { get; set; }
    }

    public class StatReq<TStat, TValue>
    {
        public int OperationType { get; set; }
        public string Message { get; set; }
        public TStat Stat { get; set; }
        public TValue Value { get; set; }
    }

    public class RecipeMod
    {
        public List<StatMod<int, int>> IntRequirements { get; set; } = new List<StatMod<int, int>>();
        public List<StatMod<int, uint>> DIDRequirements { get; set; } = new List<StatMod<int, uint>>();
        public List<StatMod<int, uint>> IIDRequirements { get; set; } = new List<StatMod<int, uint>>();
        public List<StatMod<int, double>> FloatRequirements { get; set; } = new List<StatMod<int, double>>();
        public List<StatMod<int, string>> StringRequirements { get; set; } = new List<StatMod<int, string>>();
        public List<StatMod<int, int>> BoolRequirements { get; set; } = new List<StatMod<int, int>>();

        public int ModifyHealth { get; set; }
        public int ModifyStamina { get; set; }
        public int ModifyMana { get; set; }

        public int RequiresHealth { get; set; }
        public int RequiresStamina { get; set; }
        public int RequiresMana { get; set; }

        public bool Unknown7 { get; set; }
        public uint ModificationScriptId { get; set; }
        public int Unknown9 { get; set; }
        public uint Unknown10 { get; set; }
    }

    public class RecipeReq
    {
        public List<StatReq<int, int>> IntRequirements { get; set; } = new List<StatReq<int, int>>();
        public List<StatReq<int, uint>> DIDRequirements { get; set; } = new List<StatReq<int, uint>>();
        public List<StatReq<int, uint>> IIDRequirements { get; set; } = new List<StatReq<int, uint>>();
        public List<StatReq<int, double>> FloatRequirements { get; set; } = new List<StatReq<int, double>>();
        public List<StatReq<int, string>> StringRequirements { get; set; } = new List<StatReq<int, string>>();
        public List<StatReq<int, int>> BoolRequirements { get; set; } = new List<StatReq<int, int>>();
    }

    public class RecipeChange : ChangeEntry<Recipe>
    {
        public const string TypeName = "recipe";

        [JsonIgnore]
        public override uint EntryId
        {
            get => Entry.Key;
            set => Entry.Key = value;
        }

        [JsonIgnore]
        public override string EntryType => RecipeChange.TypeName;

        [JsonProperty("recipe")]
        public override Recipe Entry { get; set; }
    }
}
