﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle;

namespace Lifestoned.DataModel.Gdle.Spawns
{
    public class SpawnWeenie
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("wcid")]
        public uint WeenieId { get; set; }

        [JsonProperty("desc")]
        public string Description { get; set; }

        [JsonProperty("pos")]
        public Position Position { get; set; }
    }
}
