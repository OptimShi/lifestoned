﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dapper;
using Newtonsoft.Json;

using Lifestoned.DataModel.Gdle.Recipes;

namespace Lifestoned.Providers.Database
{
    public class DatabaseRecipeProvider : SQLiteContentDatabase<Recipe>, IRecipeProvider
    {
		public DatabaseRecipeProvider() : base("RecipeDbConnection", "Recipes", (o) => o.Key)
        {
        }
    }
}
