﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lifestoned.DataModel.Gdle.Recipes;

namespace Lifestoned.Providers
{
    public interface IRecipeProvider : IGenericContentProvider<Recipe>
    {
        //IQueryable<Recipe> Get();
        //Recipe Get(uint id);
        //bool Save(Recipe recipe);
        //bool Update(Recipe recipe);
        //bool Delete(uint id);

        //List<SpawnMapEntry> SearchLandblocks(string query);
        //IEnumerable<SpawnMapEntry> GetLandblocks();
    }

    public interface IRecipeSandboxProvider : IRecipeProvider
    {
        RecipeChange GetChange(uint id);
        RecipeChange GetChange(uint id, string userId);
    }
}
