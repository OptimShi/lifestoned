﻿/// <reference path="../../../Scripts/jquery-3.2.1.js" />
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-spawn-map', {
    props: [ 'item' ],
    data() {
        return {
            // the whole interface has to be defined
            mapdata: this._assign(this.item),

            currentWeenie: null,
            item_modal_id: null,
            link_modal_id: null,

            link_type: 1,
            link_weenies: [],
            link_generator: 0
        };
    },
    computed: {
        shown: function () { return this.mapdata ? true : false; },

        weenies: function () { return this.mapdata ? this.mapdata.Value.Weenies : []; },
        links: function () { return this.mapdata ? this.mapdata.Value.Links : []; },

        currentLinks: function () {
            if (!this.currentWeenie) return [];
            return this.links.filter(
                (link) => link.Source == this.currentWeenie.Id || link.Target == this.currentWeenie.Id);
        }
    },
    watch: {
        item: function (nval, oval) {
            this.mapdata = this._assign(nval);
        },

        //weenies: function (nval, oval) {
        //    this.$nextTick(
        //        () => $('input.weenie-select', this.$el).weenieFinder());
        //            //.weenieFinder({ appendTo: `#${this.item_modal_id} .modal-body .panel:first-child` }));
        //},

        //link_type: function (nval, oval) {
        //    this.link_weenies = nval == 1 ? [] : -1;
        //}
    },
    methods: {
        _assign(item) {
            var result = item ? Object.assign(
                {},
                { ...item },
                {
                    Value: {
                        Weenies: item.Value ? [...item.Value.Weenies] : [],
                        Links: item.Value ? [...item.Value.Links] : []
                    }
                }) : null;
            //console.log(result);
            return result;
        },

        show() {
            this.currentWeenie = null;
            //this.$forceUpdate();
            $('#' + this.item_modal_id).modal('show');
        },

        hide() {
            $('#' + this.item_modal_id).modal('hide');
        },

        addLink() {
            if (this.currentWeenie == null) {
                alert('Please select a weenie for creating links');
                return;
            }
            this.link_type = 1;
            $('#' + this.link_modal_id).modal('show');
        },

        closeLink() {
            $('#' + this.link_modal_id).modal('hide');
        },

        delLink(link, idx) {
            var gidx = this.links.findIndex(
                (item) => item.Target == link.target || item.Source == link.Source);

            this.mapdata.Value.Links.splice(gidx, 1);
            //this.currentLinks.splice(idx, 1);
        },

        saveLinks() {
            // generator
            var src = 'Target';
            var dst = 'Source';
            var selected = this.link_weenies;
            if (parseInt(this.link_type) == 2) {
                // generated
                src = 'Source';
                dst = 'Target';
                selected = [this.link_generator];
            }

            $.each(selected, (idx, i) => {
                var ow = this.weenies.find((o) => o.Id == i);
                var o = {};
                o[src] = this.currentWeenie.Id;
                o[dst] = i;
                o.Description = `${ow.Description} -> ${this.currentWeenie.Description}`;
                this.mapdata.Value.Links.push(o);
            });

            this.select(this.currentWeenie);
            this.closeLink();
        },

        select(item) {
            this.currentWeenie = item;
        },

        addWeenie() {
            var max = this.weenies && this.weenies.length > 0 ? Math.max(...this.weenies.map(i => i.Id)) + 1 : 0;
            this.mapdata.Value.Weenies.push({
                Id: max,
                WeenieId: null,
                Description: null,
                Position: {
                    Display: null
                }
            });
            this.$nextTick(() => $('.weenies').scrollTop($('.weenies').height() + 100));
        },

        delWeenie(item, idx) {
            this.mapdata.Value.Weenies.splice(idx, 1);
            this.mapdata.Value.Links = this.links.filter(
                (link) => !(link.Target == item.Id || link.Source == item.Id));
        },

        saveItem() {
            this.$emit('save', this.mapdata);
        },

        getDesc(item) {
            var id = item.Source;
            if (item.Source == this.currentWeenie.Id) {
                id = item.Target;
            }

            var ow = this.weenies.find((o) => o.Id == id);
            if (ow)
                return ow.Description;
            return null;
        }

    },
    created() {
    },
    mounted() {
        this.item_modal_id = this._uid + '_item';
        this.link_modal_id = this._uid + '_link';
    },
    destroyed() {
    },
    updated() {
    },
    template: `
    <div :id="item_modal_id" class="modal spawn-map" data-backdrop="static">
        <div v-if="shown" class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Edit Spawn Map ({{ mapdata.Key }})</h4>
                </div>
                <div class="modal-body">
                    <div class="row row-spacer">
                        <input v-model="mapdata.Description" type="text" class="form-control input-sm" placeholder="Enter Description" />
                    </div>

                    <div class="row">
                        <div class="col-md-6">

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a>Weenies</a>
                                    <button @click="addWeenie" type="button" class="pull-right btn btn-xs btn-default"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>

                                <div class="panel-body weenies">

                                    <div v-for="(weenie, idx) in weenies" :key="mapdata.Key + '-' + idx + '-' + weenie.Id" v-on:click="select(weenie)" :selected="currentWeenie && weenie.Id == currentWeenie.Id">
                                        <div class="input-group input-group-sm row-spacer">
                                            <span class="input-group-addon">({{weenie.Id}})</span>
                                            <input v-lsd-weenie-finder v-model="weenie.WeenieId" type="text" class="form-control input-sm weenie-select" placeholder="Weenie" />
                                            <span class="input-group-btn">
                                                <button @click="delWeenie(weenie, idx)" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                                            </span>
                                        </div>
                                        <div class="input-group input-group-sm row-spacer">
                                            <span class="input-group-addon">Desc</span>
                                            <input v-model="weenie.Description" type="text" class="form-control input-sm" />
                                        </div>
                                        <div class="input-group input-group-sm row-spacer">
                                            <span class="input-group-addon">Pos</span>
                                            <input v-model="weenie.Position.Display" type="text" class="form-control input-sm" />
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <a>Links</a>
                                    <button @click="addLink" type="button" class="btn btn-xs btn-default pull-right"><i class="glyphicon glyphicon-plus"></i></button>
                                </div>

                                <div class="panel-body" style="overflow: auto; height: 55vh; padding-right: 1.5rem;">

                                    <div v-for="(link, idx) in currentLinks" class="row row-spacer">
                                        <div class="input-group input-group-sm">
                                            <span v-if="link.Target == currentWeenie.Id" class="input-group-addon">
                                                <i class="glyphicon glyphicon-triangle-right"></i>&nbsp;{{link.Source}}
                                            </span>
                                            <span v-if="link.Source == currentWeenie.Id" class="input-group-addon">
                                                <i class="glyphicon glyphicon-triangle-top"></i>&nbsp;{{link.Target}}
                                            </span>

                                            <input :value="getDesc(link)" type="text" class="form-control input-sm" />

                                            <span class="input-group-btn">
                                                <button @click="delLink(link, idx)" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div :id="link_modal_id" class="modal" data-backdrop="static">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header"><h4 class="modal-title">Add Link</h4></div>
                                <div class="modal-body">
                                    <div class="row row-spacer">
                                        <div class="col-md-3">Type</div>
                                        <div class="col-md-5">
                                            <select v-model="link_type" class="form-control">
                                                <option value="1">Generated</option>
                                                <option value="2">Generator</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row row-spacer">
                                        <div class="col-md-3">Weenies</div>
                                        <div class="col-md-9">
                                            <select v-if="link_type == 1" v-model="link_weenies" size="15" multiple class="form-control">
                                                <option v-for="(weenie, idx) in weenies" :value="weenie.Id">({{ weenie.Id }}) {{ weenie.Description }}</option>
                                            </select>
                                            <select v-if="link_type == 2" v-model="link_generator" size="15" class="form-control">
                                                <option v-for="(weenie, idx) in weenies" :value="weenie.Id">({{ weenie.Id }}) {{ weenie.Description }}</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button @click="saveLinks" type="button" class="btn btn-sm btn-primary">Ok</button>
                                    <button @click="closeLink" type="button" class="btn btn-sm btn-default">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button @click="saveItem" type="button" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    `
});