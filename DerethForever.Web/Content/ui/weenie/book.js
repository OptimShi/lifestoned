﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-book-page', {
    props: ['page'],
    model: { prop: 'page', event: 'changed' },
    computed: {
    },
    methods: {
        deleted() {
            this.$emit('deleted', this.page);
        }
    },
    template: `
    <div>
        <div class="row row-spacer">
            <div class="col-md-3">Author Name</div>
            <div class="col-md-3">Account</div>
            <div class="col-md-3">Id</div>
            <div class="col-md-2">Ignore</div>
            <div class="col-md-1">
                <button @click="deleted" type="button" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </div>
        <div class="row row-spacer">
            <div class="col-md-3"><input v-model="page.AuthorName" type="text" class="form-control" /></div>
            <div class="col-md-3"><input v-model="page.AuthorAccount" type="text" class="form-control" /></div>
            <div class="col-md-3"><input v-model="page.AuthorId" type="text" class="form-control" /></div>
            <div class="col-md-2"><input v-model="page.IgnoreAuthor" type="checkbox" class="form-control" /></div>

        </div>
        <div class="row row-spacer">
            <div class="col-md-12"><textarea v-model="page.PageText" rows="8" class="form-control"></textarea></div>
        </div>
    </div>
    `
});

Vue.component('lsd-book', {
    props: ['book'],
    model: { prop: 'book', event: 'changed' },
    data() {
        return {
            newPart: null
        };
    },
    methods: {
        hashCode(obj) {
            this.hashCode.count = this.hashCode.count || 1;
            return this.hashCode.count++;
        },
        newItem() {
            var book = {
                Pages: [],
                MaxNumberPages: null,
                MaxCharactersPerPage: null
            };
            this.$emit('changed', book);
        },
        addNew() {
            this.book.Pages.push({
                AuthorName: null,
                AuthorAccount: null,
                AuthorId: null,
                IgnoreAuthor: null,
                PageText: null
            });
        },
        deleted(page) {
            var idx = this.book.Pages.indexOf(page);
            if (idx >= 0) {
                this.book.Pages.splice(idx, 1);
            }
        }
    },
    template: `
    <lsd-panel title="Book" showAdd @adding="addNew">
        <div v-if="!book">
            <div class="col-md-4 col-md-offset-4"><button @click="newItem" type="button" class="btn btn-lg btn-default">Create Book</button></div>
        </div>
        <div v-else>
        <div class="row row-spacer">
            <div class="col-md-2">Max Pages</div>
            <div class="col-md-2"><input v-model.number="book.MaxNumberPages" type="text" class="form-control" /></div>
            <div class="col-md-2">Max Characters</div>
            <div class="col-md-2"><input v-model.number="book.MaxCharactersPerPage" type="text" class="form-control" /></div>
        </div>
        <hr />

        <ul class="nav nav-stacked col-md-2">
            <li v-for="(page, idx) in book.Pages" :key="hashCode(page)" :class="{ 'active' : (idx == 0) }"><a :href="'#w_pg_'+idx" data-toggle="tab">{{ idx }}</a></li>
        </ul>
        <div class="tab-content col-md-10">
            <div v-for="(page, idx) in book.Pages" :id="'w_pg_'+idx" class="tab-pane" :class="{ 'active' : (idx == 0) }">
                <lsd-book-page v-model="book.Pages[idx]" @deleted="deleted"></lsd-book-page>
            </div>
        </div>
        </div>
    </lsd-panel>
    `
});
