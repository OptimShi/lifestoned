﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.filter('toDate', function (value) {
    var d = value;
    if (!d) {
        return 'n/a';
    }
    if (typeof value === 'string') {
        // ms-json
        d = parseInt(value.substring(6, value.length - 2));
    }
    if (typeof d === 'number') {
        d = new Date(d);
    }
    return `${d.getFullYear()}-${d.getMonth()}-${d.getDate()}`;
});

Vue.filter('f2', function (value) {
    var d = value;
    if (!d && d !== 0) {
        return 'NaN';
    }
    if (typeof value === 'string') {
        // ms-json
        d = parseFloat(value);
    }
    return d.toFixed(2);
});