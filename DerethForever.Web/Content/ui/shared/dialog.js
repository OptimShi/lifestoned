﻿/// <reference path="../../../Scripts/jquery-3.2.1.js" />
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-dialog', {
    props: ['title'],
    methods: {
        show() {
            $('#_' + this._uid).modal('show');
        },

        hide() {
            $('#_' + this._uid).modal('hide');
        },

        saved() {
            this.$emit('saved');
            this.hide();
        }

    },
    template: `
    <div :id="'_'+_uid" class="modal" data-backdrop="static">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ title }}</h4>
                </div>
                <div class="modal-body">
                <slot></slot>
                </div>
                <div class="modal-footer">
                    <button @click="saved" type="button" class="btn btn-sm btn-primary">Save</button>
                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    `
});