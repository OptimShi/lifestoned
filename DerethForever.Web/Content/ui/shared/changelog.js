﻿
/// <reference path="../../../Scripts/vue.js" />

Vue.component('lsd-changelog', {
    props: ['item'],
    model: { prop: 'item', event: 'changed' },
    data() {
        return {};
    },
    methods: {
    },
    created() {
        mapWatchers(this, 'item', 'changed');
    },
    template: `
    <lsd-panel title="Change Log" stickHeader>
        <div class="row row-spacer">
            <div class="col-md-2">Changelog Entry</div>
            <div class="col-md-8"><textarea v-model="item.userChangeSummary" required class="form-control" rows="6"></textarea></div>
        </div>

        <hr />

        <div v-for="entry in item.Changelog" class="row row-spacer">
            <div class="col-md-10">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        {{ entry.Author }} on {{ entry.Created | toDate }}
                    </div>
                    <div class="panel-body text-prewrap">{{ entry.Comment }}</div>
                </div>
            </div>
        </div>

    </lsd-panel>
    `
});
